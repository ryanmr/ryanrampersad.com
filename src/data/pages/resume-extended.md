---
date: "2021-11-05T00:00:00-06:00"
draft: false
title: "Resume Extended"
type: resume-extended
---

You found the extended resume section. You can refer to the [primary resume](/resume) or to [work history](/history). This page notes other details that are not as pertinent.

### Education

* Graduated from the University of Minnesota Twin Cities in 2015
* Bachelors of Computer Science from the College of Science & Engineering (CSE)
* Studied on the _Software Engineering_, _Systems_ and _Compilers_ tracks

### The Nexus

In 2011, I founded my [own small podcast network](http://thenexus.tv) and built my own small in-house podcasting studio. As of late 2021, we have recorded just over 1350 episodes across various series.

### Previous work

Previous is de-emphasized because it was a long time ago in a different industry.

#### Saint Paul Public Schools

Previous work during college.

I helped instructors teach various Saint Paul Public Schools Community Education classes, such as Computer Basics 101, 201, Excel, PowerPoint and more. I also taught iterations of my own Website Basics and Website Construction classes, which focused on writing HTML, CSS and basic JavaScript by hand.
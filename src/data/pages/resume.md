---
date: "2020-09-26T00:00:00-06:00"
draft: false
title: "Resume"
type: resume
---

Follow me on Twitter [@ryanmr](https://twitter.com/ryanmr?resume).

I work at Daugherty as **Senior Software Engineer and Architect** and as **Senior Consultant**. 

My primary role is software engineering with a fullstack approach and pragmatic philosophy. Beyond that, I help business teams thrive with technology, build engineering teams focused on growth and autonomy, and lend an engineering perspective to management and its pedestrian duties. If you are interested in more details, please read the [historical record of my work](/history).

I like emoji. 🚀 🎋 🔴

---

### Breakdown

I have experience with the following to varying levels. I consider these buzzwords and basically meaningless without the context of the work. Despite that, some organizations believe buzzwords buy sales &mdash; read the following buzzwords as an exercise in asking useful questions. I cannot share details publicly about the projects that used these specific technologies, though in confidence I _may_ speak more about them privately.

#### Languages

* JavaScript
  * TypeScript
  * ES6+
* Java
* PHP
* Python
* Rust

#### Markup

* JSX
* HTML5
* YAML
* XML

#### Style

* CSS
* TailwindCSS
* SASS
* Bulma
* Bootstrap
* Foundation

#### JavaScript

* React
  * React Router
  * React Native
  * Redux
  * `react-query`
  * `create-react-app`
  * `next`
* Vue
  * Vue CLI
  * Vue Router
  * Vuex
* Node
* Axios
* Lodash
* D3
* Service Workers

#### Prehistoric JavaScript

* AngularJS
* Browserify
* Gulp
* jQuery
* Knockout
* MooTools

#### Java

* Spring
  * Spring
  * Spring Boot
  * Spring Security
  * Spring Security OAuth2
  * Spring Data
  * Data Rest
* Jackson JSON
* Android
* Maven
* Gradle

#### Python

* numpy
* scipy
* scikit learn

#### PHP

* Laravel
* WordPress
* Slim

#### CI/CD

* Azure
  * Azure DevOps
  * Azure Pipelines
* Vela
* Drone
* Bitbucket Pipelines
* GitLab CI/CD
* Jenkins

#### Design

* Excalidraw
* Balsamiq
* Axure

#### Database

* PostgreSQL
* MySQL
* Redis

#### Tools

* VS Code
* Sublime Text
* IntelliJ
* CLion
* DataGrip
* TablePlus
* iTerm2
* Insomnia
* Git

#### Communications

* Slack
* Teams
* Zoom
* Webex

#### VCS

* GitHub
* BitBucket
* GitLab

#### Organization & Planning

* GitHub Issues
* Zenhub
* Trello
* Kanban Flow
* Jira
* VSTS

#### Cloud

* AWS
  * EC2
  * Elastic Beanstalk
  * Route 53
  * Cloudfront
  * S3
  * SES
* Azure
  * App Services
  * Storage Accounts
  * CDN Endpoints
* Vercel
* Terraform
* Kubernetes
* VPS
  * Linode
  * Digital Ocean

#### Auth Services

* Auth0
* Okta
* AzureAD

#### DevOps

* Continuous Integration (CI)
* Continous Deployment (CD)
* Continous Delivery

#### Blockchain

* Hyperledger Sawtooth
* Chain

#### Impressive Buzzwords & Keyword Jargon

Here's a list of meaningless out of context keyword jargon.

* Interdisciplinary Utilization
* Mentorship
* Strategic Planning
* Process Improvement
* Workflow Enhancements
* Project Implementation Analysis
* Designing Multi-Service Applications
* Troubleshooting

---

### Other

> Anyway if you want to pay me the same or more to do whatever I want to do without performance reviews then, my direct messages are open.

This is not untrue. [Inspired by Dan](https://twitter.com/dan_abramov/status/1441222247599857665).

### Education

If you are interested in some of my education details, find more details on the [sparse extended resume](/resume-extended).

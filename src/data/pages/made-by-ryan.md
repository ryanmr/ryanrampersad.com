---
date: "2016-02-17T19:17:27-06:00"
draft: false
title: "Made by Ryan"
type: made-by-ryan
---

You can visit the [Gitlab repository](https://gitlab.com/ryanmr/ryanrampersad.com). Changes are pushed to the repository, built by Gitlab and finally, deployed here.

#### Tech

This site uses Gatsby as its static site generator. Gatsby offers the best-in-class pattern for building on the web today. React provides the component based composition pattern that makes interface building a breeze. Styled Components addresses modern styling needs with capabilities for optimized bundles.

* [gatsby](https://www.gatsbyjs.org/)
* [react](https://reactjs.org/)
* [styled-components](https://www.styled-components.com/)

#### Font

[League Gothic](https://www.theleagueofmoveabletype.com/league-gothic). Uppercased, text looks handsome and proud, but lean and agile. Featured as my name.

<small>_What I wouldn't give a modern and refreshed League Gothic?_</small>

#### Ribbon

I was inspired by [Evan You's](http://evanyou.me/) canvas effect. While it is no longer used on his website, I keep an _as-was_ code copy of the original effect [in this gist](https://gist.github.com/ryanmr/205ef4297e7821fad088).

Based on the concept of those _triangles_ <span title="à la growth">climbing from left to right</span>, I added additional extensions like _red_ for _branding_ and <span title="aeternus et umquam">the _motion_</span>.


[ryanrampersad.com](https://ryanrampersad.com?gitlab)
===================

Welcome to the Ryan Rampersad static site! I periodically update the site with my resume, work history and accomplishments, referral links, links to my other homes online and more.

## Tech

This static site was created with [Gatsby](https://www.gatsbyjs.org/).

## Hosting

This site, [ryanrampersad.com](https://ryanrampersad.com?gitlab), is hosted by my excellent [Linode VPS](https://adept.work/refln).

Currently, the site is hosted among my other sites, through a multihost apache configuration with Let's Encrypt enabled.

## Build and Deploy

This site is built and deployed with the wonderfully free tool, Gitlab CI/CD. With Gitlab CI/CD, any push to `master` is built and then deployed within moments to my personal VPS.

## Legacy

The previous build of this site was made with the [Hugo static site generator](https://gohugo.io/). That build will remain in another branch indefinitely.